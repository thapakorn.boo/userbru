import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:userbru/homeuser.dart';
import 'package:userbru/static/customStyle.dart';

class MenuPage extends StatefulWidget {
  @override
  MenuPageState createState() => MenuPageState();
}

class MenuPageState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple[700],
        title: Text('HELLO BRU',
            style: TextStyle(color: Colors.white, fontSize: 20)),
      ),
      body: Container(
          padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.1, 1],
              colors: [Colors.purple.shade200, Colors.pink.shade200],
            ),
          ),
          child: ListView(
            children: <Widget>[
              Builder(
                builder: (context) => Center(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 80),
                      ),
                      Container(
                        width: 300.0,
                        height: 300.0,
                        child: Image.asset('images/bus.png'),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 1),
                        padding: EdgeInsets.all(1),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                          child: Column(
                        children: <Widget>[
                          CustomStyle.createbutton(
                              title: 'BUS BRU',
                              color: Colors.purple,
                              shadowColor: Colors.black,
                              textColor: Colors.white,
                              function: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => HomePageWidget()));
                              },
                              width: 250),
                          SizedBox(
                            height: 20,
                          ),
                          CustomStyle.createbutton(
                              title: 'AR BRU',
                              color: Colors.purple,
                              shadowColor: Colors.black,
                              textColor: Colors.white,
                              function: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => HomePageWidget()));
                              },
                              width: 250),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      )),
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}

void arbru() {}

void busbru() {}
