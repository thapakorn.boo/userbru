import 'package:flutter/material.dart';

import 'package:loading_animations/loading_animations.dart';
import 'package:userbru/menu.dart';
import 'package:userbru/register.dart';
import 'package:userbru/service/APIService.dart';
import 'static/customStyle.dart';

class UserLoginPage extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<UserLoginPage> {
  final focus = FocusNode();
  final idcontroller = TextEditingController();
  final passcontroller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool _loading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Login'),
      backgroundColor: Colors.purple[700],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.1, 1],
                colors: [Colors.purple.shade200, Colors.pink.shade200],
              ),
            ),
            child: ListView(
              children: <Widget>[
                Builder(
                  builder: (context) => Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 5),
                        ),
                        Container(
                          width: 250.0,
                          height: 250.0,
                          child: Image.asset('images/bus.png'),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.all(20),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                TextFormField(
                                  decoration: InputDecoration(
                                    fillColor: Colors.white,
                                    hoverColor: Colors.white,
                                    hintText: 'ชื่อผู้ใช้',
                                    labelText: 'ชื่อผู้ใช้',
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(32.0)),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 15.0, horizontal: 10.0),
                                  ),
                                  autocorrect: false,
                                  autovalidate: false,
                                  controller: idcontroller,
                                  validator: (value) {
                                    if (value.isEmpty)
                                      return 'Username cannot be empty.';

                                    return null;
                                  },
                                  onFieldSubmitted: (v) {
                                    FocusScope.of(context).requestFocus(focus);
                                  },
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                TextFormField(
                                    focusNode: focus,
                                    decoration: InputDecoration(
                                      hintText: 'รหัสผ่าน',
                                      labelText: 'รหัสผ่าน',
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(32.0)),
                                      contentPadding: new EdgeInsets.symmetric(
                                          vertical: 15.0, horizontal: 10.0),
                                    ),
                                    autocorrect: false,
                                    autovalidate: false,
                                    controller: passcontroller,
                                    obscureText: true,
                                    validator: (value) {
                                      if (value.isEmpty)
                                        return 'Password cannot be empty.';

                                      return null;
                                    }),
                                SizedBox(
                                  height: 20,
                                ),
                                CustomStyle.createbutton(
                                    title: 'Login',
                                    color: Colors.purple,
                                    shadowColor: Colors.black,
                                    textColor: Colors.white,
                                    function: () {
                                      login();
                                    },
                                    width: 250),
                                SizedBox(
                                  height: 20,
                                ),
                                CustomStyle.createbutton(
                                    title: 'Register',
                                    color: Colors.purple,
                                    shadowColor: Colors.black,
                                    textColor: Colors.white,
                                    function: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => RegisterPage()));
                                    },
                                    width: 250),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          _loading ? bodyProgress : SizedBox(),
        ],
      ),
    );
  }

  var bodyProgress = new Container(
    child: new Stack(
      children: <Widget>[
        new Container(
          alignment: AlignmentDirectional.center,
          decoration: new BoxDecoration(
            color: Colors.white70,
          ),
          child: new Container(
            decoration: new BoxDecoration(
                color: Colors.purple[300],
                borderRadius: new BorderRadius.circular(10.0)),
            width: 300.0,
            height: 200.0,
            alignment: AlignmentDirectional.center,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Center(
                  child: new SizedBox(
                      height: 50.0,
                      width: 50.0,
                      child: LoadingJumpingLine.square(
                        borderColor: Colors.orange.shade200,
                        borderSize: 4.0,
                        size: 40.0,
                        backgroundColor: Colors.white,
                        duration: Duration(microseconds: 500),
                      )),
                ),
                new Container(
                  margin: const EdgeInsets.only(top: 25.0),
                  child: new Center(
                    child: new Text(
                      "loading.. wait...",
                      style: new TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );

  void login() async {
    // try {
    if (_formKey.currentState.validate()) {
      setState(() {
        _loading = true;
      });
      APIService apiService = new APIService();
      var result =
          await apiService.login(idcontroller.text, passcontroller.text);
      if (result) {
        // final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
        // String token = await firebaseMessaging.getToken();
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (_) => MenuPage()));
      } else
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(apiService.message),
        ));
    }
    // } catch (e) {
    //   _scaffoldKey.currentState.showSnackBar(SnackBar(
    //     content: Text(e),
    //   ));
    // } finally {
    //   setState(() {
    //     _loading = false;
    //   });
    // }
  }
}
