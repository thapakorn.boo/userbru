import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;

class RegisterPost {
  final String name;
  final String username;
  final String pass;
  final String tel;
  final String pic;
  final String status;
  final String familyid;
  final String familyname;
  RegisterPost(
      {this.name,
      this.username,
      this.pass,
      this.tel,
      this.pic,
      this.status,
      this.familyname,
      this.familyid});
  factory RegisterPost.fromJson(Map<String, dynamic> json) {
    return RegisterPost(
        name: json['name'],
        username: json['username'],
        pass: json['pass'],
        tel: json['tel'],
        pic: json['pic'],
        status: json['status'],
        familyid: json['familyid'],
        familyname: json['familyname']);
  }

  Object get title => null;

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['name'] = name;
    map['username'] = username;
    map['pass'] = pass;
    map['tel'] = tel;
    map['pic'] = pic;
    map['status'] = status;
    map['familyid'] = familyid;
    map['familyname'] = familyname;
    return map;
  }
}

Future<RegisterPost> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return RegisterPost.fromJson(json.decode(response.body));
  });
}
