class RegisterResponse {
  int id;
  String name;
  String tel;
  String pic;
  RegisterResponse({this.id, this.name, this.tel, this.pic});
  factory RegisterResponse.fromJson(Map<String, dynamic> json) {
    return RegisterResponse(
      id: json['uid'],
      name: json['u_name'],
      tel:json['u_tel'],
      pic:json['pic']
    );
  }
}
