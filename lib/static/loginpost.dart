import 'dart:convert';
import 'package:http/http.dart' as http;

class LoginPost {
  final String username;
  final String pass;

  LoginPost({this.username, this.pass});
  factory LoginPost.fromJson(Map<String, dynamic> json) {
    return LoginPost(
      username: json['username'],
      pass: json['pass'],
    );
  }

  Object get title => null;

  Map toMap() {
    var map = new Map<String, dynamic>();
    map['username'] = username;
    map['pass'] = pass;
    return map;
  }
}

Future<LoginPost> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;

    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return LoginPost.fromJson(json.decode(response.body));
  });
}
