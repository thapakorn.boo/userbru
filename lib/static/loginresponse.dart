class LoginResponse {
  int id;
  String name;
  String tel;
  String picture;
  LoginResponse({this.id, this.name, this.tel, this.picture});
  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
        id: json['uid'],
        name: json['u_name'],
        tel: json['u_tel'],
        picture: json['pic']);
  }
}
