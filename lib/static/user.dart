class User {
  final int uid;
  final String username;
  final String name;
  final String tel;

  User({this.uid, this.username, this.name, this.tel});
  
  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      uid: json['uid'],
      username: json['username'],
      name: json['u_name'],
      tel: json['u_tel'],
    );
  }
  
  Map toMap() {
    var map = new Map<String, dynamic>();
    map['uid'] = uid;
    map['username'] = username;
    map['u_name'] = name;
    map['u_tel'] = tel;
    return map;
  }
}
