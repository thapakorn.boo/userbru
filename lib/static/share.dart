import 'package:shared_preferences/shared_preferences.dart';

class SharedData {
  /////////////////////////////
  Future<int> getId() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getInt('id');
    return result;
  } //เพิ่ม

  Future<void> removeId() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('id');
  }

  Future<void> setId(int id) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setInt('id', id);
  }

/////////////////////////
  Future<String> getName() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getString('name');
    return result;
  } //เพิ่ม

  Future<void> removeName() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('name');
  }

  Future<void> setName(String name) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('name', name);
  }

//////////////////////
  Future<String> getTel() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getString('tel');
    return result;
  } //เพิ่ม

  Future<void> removeTel() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('tel');
  }

  Future<void> setTel(String tel) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('tel', tel);
  }

  Future<bool> getIsLogged() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getBool('isLogged');
    return result;
  } //เพิ่ม

  Future<void> removeIsLogged() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('isLogged');
  }

  Future<void> setIsLogged(bool isLogged) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isLogged', isLogged);
  }

  Future<void> removeEveryThing() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('id');
    await prefs.remove('name');
    await prefs.remove('tel');
    await prefs.remove('isLogged');
  }

  getToken(String token) {}

/////////////////////////////////

}
