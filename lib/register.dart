import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:userbru/menu.dart';
import 'package:userbru/service/APIService.dart';


import 'Static/customStyle.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RegisterPageWidget();
  }
}

class RegisterPageWidget extends StatefulWidget {
  _RegisterPageWidget createState() => _RegisterPageWidget();

  void setState(Null Function() param0) {}
}

enum SingingCharacter { Parent, Child }

class _RegisterPageWidget extends State<RegisterPageWidget> {
  SingingCharacter _character = SingingCharacter.Parent;
  final usernamecontroller = TextEditingController();
  final passcontroller = TextEditingController();
  final namecontroller = TextEditingController();
  final confirmpasswordcontroller = TextEditingController();
  final telcontroller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String _value;

  @override
  Widget build(BuildContext context) {
    print(_value);
    return Scaffold(
      appBar: AppBar(
    backgroundColor: Colors.purple[700],
        title: Text('Register'),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 5, 30, 0),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.1, 1],
            colors: [Colors.purple.shade200, Colors.pink.shade200],
          ),
        ),
        child: ListView(
          children: <Widget>[
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Center(
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Username',
                        labelText: 'Username',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                        contentPadding: new EdgeInsets.symmetric(
                            vertical: 15.0, horizontal: 10.0),
                      ),
                      autocorrect: false,
                      autovalidate: false,
                      controller: usernamecontroller,
                      validator: (value) {
                        if (value.isEmpty) return 'Username cannot be empty.';
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Name',
                          labelText: 'Name',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                        ),
                        autocorrect: false,
                        autovalidate: false,
                        controller: namecontroller,
                        validator: (value) {
                          if (value.isEmpty) return 'Name cannot be empty.';
                          return null;
                        }),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Password',
                          labelText: 'Password',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                        ),
                        autocorrect: false,
                        autovalidate: false,
                        controller: passcontroller,
                        obscureText: true,
                        validator: (value) {
                          if (value.isEmpty) return 'Password cannot be empty.';
                          if (value != confirmpasswordcontroller.text)
                            return 'Password doesn\'t match.';
                          return null;
                        }),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Confrim Password',
                          labelText: 'Confrim Password',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                        ),
                        autocorrect: false,
                        autovalidate: false,
                        controller: confirmpasswordcontroller,
                        obscureText: true,
                        validator: (value) {
                          if (value.isEmpty)
                            return 'Confirm password cannot be empty.';
                          if (value != passcontroller.text)
                            return 'Password doesn\'t match.';

                          return null;
                        }),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                        decoration: InputDecoration(
                          hintText: 'Telephone',
                          labelText: 'Telephone',
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0)),
                          contentPadding: new EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                        ),
                        keyboardType: TextInputType.number,
                        autocorrect: false,
                        autovalidate: false,
                        controller: telcontroller,
                        validator: (value) {
                          if (value.isEmpty)
                            return 'Telephone cannot be empty.';
                          return null;
                        }),
                    SizedBox(
                      height: 10,
                    ),
                    CustomStyle.createbutton(
                        title: 'Register',
                        color: Colors.orange.shade200,
                        shadowColor: Colors.pink,
                        textColor: Colors.white,
                        function: () {
                          register();
                        },
                        width: 250),
                    SizedBox(
                      height: 40.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void register() async {
    try {
      if (_formKey.currentState.validate()) {
        String username = usernamecontroller.text;
        String name = namecontroller.text;
        String pass = passcontroller.text;
        String tel = telcontroller.text;

        APIService apiService = new APIService();
        var result;
        if (_value == 'new') {
          result = await apiService.register(
            username,
            pass,
            name,
            tel,
          );
        } else {
          result = await apiService.register(
            username,
            pass,
            name,
            tel,
          );
        }
        if (result) {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (_) => MenuPage()));
        } else {
          print(apiService.message);
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
