import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:userbru/static/loginpost.dart';
import 'package:userbru/static/loginresponse.dart';
import 'package:userbru/static/registerpost.dart';
import 'package:userbru/static/registerresponse.dart';
import 'package:userbru/static/share.dart';


class APIService {
  static String ip = "http://192.168.46.221:8080";

  String message;
  final String usernametest = "trill";
  final String passwordtest = "1";
  get firebaseMessaging => null;
  Future<bool> login(String username, String pass) async {
    if (username == this.usernametest) {
      if (pass == this.passwordtest) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }

    try {
      LoginPost loginPost = new LoginPost(username: username, pass: pass);
      var url = ip + '/apidriver/login';

      var body = json.encode(loginPost.toMap());

      var response = await http
          .post(url, headers: {"Content-Type": "application/json"}, body: body)
          .timeout(Duration(seconds: 5));
      print(response);
      if (response.statusCode == 200) {
        var data = LoginResponse.fromJson(
            json.decode(utf8.decode(response.bodyBytes)));
        final storedata = SharedData();
        storedata.setId(data.id);
        storedata.setName(data.name);
        storedata.setTel(data.tel);
        storedata.setIsLogged(true);
        print(storedata);
        return true;
      } else {
        message = response.body;
        return false;
      }
    } catch (e) {
      message = e;
      return false;
    }
  }

  Future<bool> register(String username, String pass, String name, String tel)
      async {
    try {
      RegisterPost registerPost = new RegisterPost(
        name: name,
        username: username,
        pass: pass,
        tel: tel,
      );
      var url = ip + '/api/register';

      var body = json.encode(registerPost.toMap());

      var response = await http
          .post(url, headers: {"Content-Type": "application/json"}, body: body)
          .timeout(Duration(seconds: 5));
      if (response.statusCode == 200) {
        var data = RegisterResponse.fromJson(
            json.decode(utf8.decode(response.bodyBytes)));
        final storedata = SharedData();
        storedata.setId(data.id);
        storedata.setName(data.name);
        storedata.setTel(data.tel);
        return true;
      } else {
        message = response.body;
        return false;
      }
    } catch (e) {
      message = e;
      return false;
    }
  }

  Future<dynamic> getProfile(String ufid) async {
    try {
      var url = ip + '/user/getprofile';
      var map = new Map<String, dynamic>();
      map['ufid'] = ufid;
      var body = json.encode(map);
      var response = await http
          .post(url, headers: {"Content-Type": "application/json"}, body: body)
          .timeout(Duration(seconds: 5));
      if (response.statusCode == 200) {
        List<dynamic> data = jsonDecode(response.body);
        return data;
      } else {
        message = response.body;
        return false;
      }
    } catch (e) {
      message = e;
      return false;
    }
  }

  Future<dynamic> createFamily(String name, String status) async {
    try {
      SharedData data = new SharedData();
      int id = await data.getId();

      var url = ip + '/api/addfamily';
      var map = new Map<String, dynamic>();
      map['id'] = id;
      map['name'] = name;
      map['status'] = status;
      var body = json.encode(map);
      var response = await http
          .post(url, headers: {"Content-Type": "application/json"}, body: body)
          .timeout(Duration(seconds: 5));
      if (response.statusCode == 200) {
        message = "Create family successful.";
        return true;
      } else {
        message = response.body;
        return false;
      }
    } catch (e) {
      message = e;
      return false;
    }
  }

  Future<dynamic> joinFamily(String fid, String status) async {
    try {
      SharedData data = new SharedData();
      int id = await data.getId();

      var url = ip + '/user/joinfamily';
      var map = new Map<String, dynamic>();
      map['id'] = id;
      map['fid'] = fid;
      var body = json.encode(map);
      var response = await http
          .post(url, headers: {"Content-Type": "application/json"}, body: body)
          .timeout(Duration(seconds: 5));
      if (response.statusCode == 200) {
        message = "Join family successful.";
        return true;
      } else {
        message = response.body;
        return false;
      }
    } catch (e) {
      message = e;
      return false;
    }
  }

  Future<dynamic> setToken() async {
    try {
      SharedData data = new SharedData();
      int id = await data.getId();
      String token = await firebaseMessaging.getToken();
      var url = ip + '/api/settokenfcm';
      var map = new Map<String, dynamic>();
      map['uid'] = id;
      map['token'] = token;
      var body = json.encode(map);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"}, body: body);
      if (response.statusCode == 200) {
        return true;
      } else {
        message = response.body;
        return false;
      }
    } catch (e) {
      message = e;
      return false;
    }
  }

  Future getuserlocatoin(String fid) async {
    try {
      var url = ip + '/api/getuserlocation';
      var map = new Map<String, dynamic>();
      map['fid'] = fid;
      var body = json.encode(map);
      var response = await http.post(url,
          headers: {"Content-Type": "application/json"}, body: body);
      if (response.statusCode == 200) {
        List<dynamic> data = jsonDecode(response.body);
        return data;
      } else {
        message = response.body;
        return false;
      }
    } catch (e) {}
  }

  setLocation(String string, double geolat, double geolng) {}
}
