import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

import 'package:splashscreen/splashscreen.dart';
import 'package:userbru/login.dart';
import 'package:userbru/service/LocalService.dart';
import 'package:userbru/static/share.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppPage createState() => new MyAppPage();
}

class MyAppPage extends State<MyApp> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  initState() {
    initFirebaseMessaging();
    super.initState();
  }

  void initFirebaseMessaging() async {
    _firebaseMessaging.getToken().then((token) {
      print(token);
      saveToken(token);
      LocalService.checkToken(token);
    });
    _firebaseMessaging.onTokenRefresh;
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  void saveToken(String token) {
    SharedData data = new SharedData();
    data.getToken(token);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: Container(
          child: SplashScreen(
              seconds: 5,
              image: new Image.asset('images/bus.png', scale: .1),
              navigateAfterSeconds: new UserLoginPage(),
              title: new Text(
                'HELLO BRU',
                style:
                    new TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),
              ),
              loadingText: Text('Loading'),
              gradientBackground: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.1, 1],
                colors: [Colors.purple.shade200, Colors.pink.shade200],
              ),
              styleTextUnderTheLoader: new TextStyle(),
              photoSize: 100.0,
              onClick: () => print("Flutter Egypt"),
              loaderColor: Colors.deepPurple)),
    );
  }
}
