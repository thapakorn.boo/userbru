import 'dart:async';

import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:geolocator/geolocator.dart';
import 'package:geolocator/geolocator.dart' as Go;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:image/image.dart' as Images;
import 'package:userbru/service/APIService.dart';
import 'package:userbru/static/share.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return (HomePageWidget());
  }
}

class HomePageWidget extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePageWidget> {
  List<Marker> allMarkers = [];
  LocationData currentLocation;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _loading = false;
  bool first = true;
  String exisitfid;

  @override
  void initState() {
    super.initState();
    call();
  }

  Future<void> setLocation(double geolat, double geolng) async {
    APIService api = new APIService();
    SharedData a = new SharedData();
    int i = await a.getId();
    await api.setLocation(
      i.toString(),
      geolat,
      geolng,
    );
  }

  void call() async {
    try {
      setState(() {
        _loading = true;
      });
      var geolocator = Geolocator();
      var locationOptions = LocationOptions(
          accuracy: Go.LocationAccuracy.high, distanceFilter: 10);

      geolocator
          .getPositionStream(locationOptions)
          .listen((Position position) async {
        if (position != null) {
          await setLocation(position.latitude, position.longitude);
        }
      });

      APIService apiService = new APIService();

      setState(() {});
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        _loading = false;
      });
    }
  }

  GoogleMapController mapController;

  @override
  void dispose() {
    print('Dispose');
    super.dispose();
  }

  void onMapCreated(controller) {
    setState(() {
      mapController = controller;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('เลือกเส้นทางที่คุณต้องการ'),
        backgroundColor: Colors.purple[700],
      ),
      body: Stack(children: [
        Container(
          child: GoogleMap(
            myLocationEnabled: true,
            initialCameraPosition: CameraPosition(
                target: LatLng(
                  14.9893579,
                  103.09912,
                ),
                zoom: 18.0),
            markers: Set<Marker>.of(allMarkers),
            onMapCreated: onMapCreated,
          ),
        ),
        Center(
          child: Column(
            children: <Widget>[],
          ),
        ),
      ]),
    );
  }

  // Future<void> onFamilyChange(String fid) async {
  //   try {
  //     setState(() {
  //       _loading = true;
  //     });
  //     if (exisitfid == null)
  //       setState(() {
  //         exisitfid = fid;
  //         first = true;
  //       });
  //     else if (exisitfid != fid) {
  //       setState(() {
  //         exisitfid = fid;
  //         first = true;
  //       });
  //     } else
  //       setState(() {
  //         first = false;
  //       });
  //     APIService api = new APIService();
  //     FamilyLocationList data = await api.getuserlocatoin(fid);
  //     List<Marker> list = [];
  //     setState(() {
  //       allMarkers.clear();
  //     });
  //     for (int i = 0; i < data.data.length; i++) {
  //       final int targetWidth = 200;
  //       final File markerImageFile = await DefaultCacheManager().getSingleFile(
  //           APIService.ip +
  //               "/user/getimage/" +
  //               data.data[i].uid.uid.toString());
  //       final Uint8List markerImageBytes = await markerImageFile.readAsBytes();
  //       final ui.Codec markerImageCodec = await ui.instantiateImageCodec(
  //         markerImageBytes,
  //         targetWidth: targetWidth,
  //       );
  //       final ui.FrameInfo frameInfo = await markerImageCodec.getNextFrame();
  //       final ByteData byteData = await frameInfo.image.toByteData(
  //         format: ui.ImageByteFormat.png,
  //       );
  //       final Uint8List resizedMarkerImageBytes = byteData.buffer.asUint8List();

  //       var img = await makeReceiptImage(
  //           resizedMarkerImageBytes, data.data[i].uid.uid, 80);

  //       list.add(Marker(
  //         icon: BitmapDescriptor.fromBytes(img),
  //         markerId: MarkerId(data.data[i].uid.uid.toString()),
  //         position: LatLng((data.data[i].lat), (data.data[i].lng)),
  //         infoWindow: InfoWindow(title: data.data[i].uid.name),
  //       ));
  //       img = await makeReceiptImage(
  //           resizedMarkerImageBytes, data.data[i].uid.uid, 140);
  //     }

  //     if (first) {
  //       SharedData a = new SharedData();
  //       int i = await a.getId();
  //       var b = list
  //           .where((x) => x.markerId.value.toString() == i.toString())
  //           .toList();
  //       mapController.animateCamera(CameraUpdate.newCameraPosition(
  //         CameraPosition(
  //             target: b[0].position, zoom: 20.0, bearing: 45.0, tilt: 45.0),
  //       ));
  //     }

  //     new Timer.periodic(new Duration(seconds: 60), (_) => onFamilyChange(fid));
  //   } catch (e) {
  //     print(e);
  //   } finally {
  //     setState(() {
  //       _loading = false;
  //     });
  //   }
  // }

//   Future<List<int>> makeReceiptImage(Uint8List img, int id, int radius) async {
//     try {
//       List<int> bytes = img;

//       var avatarImage = Images.decodeImage(bytes);

//       //load marker image
//       SharedData data = new SharedData();
//       int existsId = await data.getId();
//       ByteData imageData;
//       if (existsId != id)
//         imageData = await rootBundle.load('images/222.png');
//       else
//         imageData = await rootBundle.load('images/555.png');

//       bytes = Uint8List.view(imageData.buffer);
//       var markerImage = Images.decodeImage(bytes);

//       //resize the avatar image to fit inside the marker image
//       avatarImage = Images.copyResize(avatarImage,
//           width: markerImage.width ~/ 1.1, height: markerImage.height ~/ 1.1);

//       int originX = avatarImage.width ~/ 2, originY = avatarImage.height ~/ 2;

//       //draw the avatar image cropped as a circle inside the marker image
//       for (int y = -radius; y <= radius; y++)
//         for (int x = -radius; x <= radius; x++)
//           if (x * x + y * y <= radius * radius)
//             markerImage.setPixelSafe(originX + x + 13, originY + y + 24,
//                 avatarImage.getPixelSafe(originX + x, originY + y));

//       return Images.encodePng(markerImage);
//     } catch (e) {
//       return null;
//     }
//   }
}
